const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const { execSync } = require('child_process');
const {stdout, stderr} = require('process');
const chokidar = require('chokidar');

app.use(express.static("site"));

let busy = false;

chokidar.watch("docs").on("all", (e, p) => {
    if(!busy)
    {
        busy = true;
        execSync("mkdocs build");
        io.sockets.emit("refresh");

        setTimeout(() => {
            busy = false;
        }, 1000);
    }
});

io.on("connection", (socket) => {
    console.log("Socket connected");
});

server.listen(5000, () => {
    console.log("The server started on port http://127.0.0.1:5000");
});
