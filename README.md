# Mkdocs Watch
This is a node app that automatically rebuilds and serves a mkdocs webite when you make changes.

## Installation

```
npm install
pip install mkdocs
```
